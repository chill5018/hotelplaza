public class Date{
   // fields 
   private int year;
   private int month;
   private int day;
   
   // constructors
   public Date(){
   
   }
   public Date(int year, int month, int day){
      setYear(year);
      setMonth(month);
      setDay(day);
   }
   public Date(Date other){
      this(other.year, other.month, other.day);
   }
   
   // methods
   public void addDays(int days){
      for(int i=1;i<=days;i++){
         nextDay();
      }
   }
   
   public int daysBetween(Date other){
      int count = 0;
      if(before(other)){
         // make a temporary date so you will not change your dates
         Date temp = new Date(this);
         while(temp.year != other.year || temp.month != other.month || temp.day != other.day){
            count++;
            temp.nextDay();
         }
      }else{
         Date temp = new Date(other);
         while(temp.year != year || temp.month != month || temp.day != day){
            count++;
            temp.nextDay();
         }
      }
      return count;
   }
   
   // returns true if a date is before other
   public boolean before(Date other){
      return year < other.year || (year == other.year && (month < other.month || month == other.month && day < other.day));
   }
   
   public void nextDay(){
      day++;
      if(day > getDaysInMonth()){
         month++;
         day=1;
         if(month > 12){
            month=1;
            year++;
         }
      }
   }
   
   public int getDaysInYear(){
      if(isLeapYear())
         return 366;
      else
         return 365;
   }
   
   public int getDaysInMonth(){
      if(month == 4 || month == 6 || month == 9 || month == 11){
         return 30;
      }else if(month == 2){
         if(isLeapYear()){
            return 29;
         }else{
            return 28;
         }
      }else{
         return 31;
      }
   }
   
   public boolean isLeapYear(){
      if((year % 4 == 0 && year % 100 != 0 ) || year % 400 == 0)
         return true;
      return false;   
   }
   
   public int getYear(){
      return year;
   }
   
   public int getMonth(){
      return month;
   }
   
   public int getDay(){
      return day;
   }
   
   public void setYear(int year){
      this.year = year;
   }
   
   public void setMonth(int month){
      if(month < 1 || month > 12){
         throw new IllegalArgumentException("Illegal month: "+month);
      }
      this.month = month;
   }
   
   public void setDay(int day){
      if(day < 1 || day > getDaysInMonth()){
         throw new IllegalArgumentException("Illegal day: "+day);
      }
      this.day = day;
   }
    
   public String toString(){
      return year+"/"+month+"/"+day;
   }
}