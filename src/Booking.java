

public class Booking {

	private Date startDate = new Date();
	private Date endDate = new Date();
	private int numOfDays;
	private Room room;
	private Guests guest;



	public Booking(){

	}

	public Booking(Date startDate, Date endDate, int numOfDays, Room room, Guests guest){
		this.startDate = startDate;
		this.endDate = endDate;
		this.numOfDays = numOfDays;
		this.room = room;
		this.guest = guest;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getNumOfDays() {
		return this.numOfDays;
	}

	public void setNumOfDays(int numOfDays) {
		this.numOfDays = numOfDays;
	}

	public void setRoom(Room room){
		this.room = room;
	}
	public int getRoom() {
		return room.getRoomID();
	}


	public void setGuest(Guests guest) {
		this.guest = guest;
	}

	public int getGuest() {
		return guest.getGuestID();

	}

	public String toString(){
		return String.format("Stat Date: %s, EndDate: %s, Days: %d, Room Number: %d, Guest ID: %d",startDate, endDate, numOfDays, room.getRoomID(), guest.getGuestID());
	}

}