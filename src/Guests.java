public class Guests {

	private int guestID = 0;
	private String firstName;
	private String lastName;
	private String address;
	private String phoneNum;

	public Guests(){
		this(0, " ", " ", " ", " ");
	}

	public Guests( int guestID, String firstName, String lastName, String address, String phoneNum){
		this.guestID = guestID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phoneNum = phoneNum;

	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}



	public int getGuestID() {
		return this.guestID;
	}

	public void setGuestID(int guestID) {
		this.guestID = guestID;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
   
   public String toString(){
      return String.format("ID: %d, Name: %s, %s, Address: %s, Phone: %s",
      guestID, firstName, lastName, address, phoneNum);
   }

}