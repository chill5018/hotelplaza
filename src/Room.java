
/**
 * This is the class that handles the rooms and the details of each room
 */
public class Room {

	private int roomID;
	private int numOfBeds;
	private Boolean internetAccess;
	private double pricePerNight;
	private int floor;


	public Room(){
		this(0,0,false,0.0,0);
	}

	public Room(int roomID, int numOfBeds, Boolean internetAccess, double pricePerNight, int floor){
		this.roomID = roomID;
		this.numOfBeds = numOfBeds;
		this.internetAccess = internetAccess;
		this.pricePerNight = pricePerNight;
		this.floor = floor;
	}

	public int getRoomID() {
		return this.roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public int getNumOfBeds() {
		return this.numOfBeds;
	}

	public void setNumOfBeds(int numOfBeds) {
		this.numOfBeds = numOfBeds;
	}

	public Boolean getInternetAccess() {
		return this.internetAccess;
	}

	public void setInternetAccess(Boolean internetAccess) {
		this.internetAccess = internetAccess;
	}

	public double getPricePerNight() {
		return this.pricePerNight;
	}

	public void setPricePerNight(double pricePerNight) {
		this.pricePerNight = pricePerNight;
	}

	public int getFloor() {
		return this.floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public String toString(){
		return String.format("Room Number: %d , Beds: %d , Internet: %s , Price: %.2f , Floor: %d", roomID, numOfBeds, internetAccess, pricePerNight, floor);
	}


}