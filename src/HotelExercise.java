import java.io.*;
import java.util.*;

/**
 * This is the Main for the project. Everything will be integrated here
 */
public class HotelExercise {

    public static Staff[] sArray = new Staff[100];
    public static int sCount = 0;

    public static Guests[] gArray = new Guests[100];
    public static int gCount = 0;

    public static Room[] rArray = new Room[100];
    public static int rCount = 0;

    public static Booking[] bArray = new Booking[100];
    public static int bCount = 0;

    public static void main(String[] args) throws Exception{
        Scanner input = new Scanner(System.in);

        menu(input);
    }

    public static void menu(Scanner input) throws Exception{
       int num = -1;
       while(num != 0){
            System.out.println("You can make the following operations:");
            Thread.sleep(300);
            System.out.println("Enter 1 to create a room.");
            Thread.sleep(300);
            System.out.println("Enter 2 to create a guest.");
            Thread.sleep(300);
            System.out.println("Enter 3 to create a staff member.");
            Thread.sleep(300);
            System.out.println("Enter 4 to create a booking.");
            Thread.sleep(300);
            System.out.println("Enter 5 to see everything you have created.");
            Thread.sleep(300);
            System.out.println("Enter 6 to extend a booking.");
            Thread.sleep(300);
            System.out.println("Enter 7 to pay for a booking.");
            Thread.sleep(300);
            System.out.println("Also, if you want to exit the menu enter 0.");
            num = input.nextInt();
            switch(num){

                case 1:
                    //Create room
                    createRoom(input);
                    break;
                case 2:
                    //Create guest
                    createGuest(input);
                    break;
                case 3:
                    //Create staff
                    createStaff(input);
                    break;
                case 4:
                    //Create booking
                    createBooking(input);
                    break;
                case 0:
                    System.out.print("Thank you for using the app");
                    break;
                case 5:
                    // Show everything you've created
                    show();
                    break;
                case 6:
                    // Extend a booking
                   // extendBooking(input);
                    break;
                case 7:
                    // Print bill
                    printBill(input);
                    break;

                default:
                    System.out.println("You have to enter 1,2,3,4,5,6 or 7 to use our app or 0 to exit the menu.");
                    break;
            }
            System.out.println();
        }
    }

    public static void createRoom(Scanner input) throws Exception{
        PrintStream fio = new PrintStream(new File("rooms.txt"));
        Room r = new Room();

        System.out.print("Enter the RoomID: ");
        r.setRoomID(input.nextInt());
        System.out.print("Enter the number of beds: ");
        r.setNumOfBeds(input.nextInt());

        System.out.print("Internet: Enter yes or no ");
        String i = input.next();


        if (i.equalsIgnoreCase("y")){
            r.setInternetAccess(true);
        } else {
            r.setInternetAccess(false);
        }

        System.out.print("Enter the price per night: ");
        r.setPricePerNight(input.nextDouble());

        System.out.print("Enter the floor: ");
        r.setFloor(input.nextInt());

        rArray[rCount] = r;
        System.out.println(r);
        rCount++;

        // print every guest from the array in the file in a raw format
        for (int j = 0; j < rArray.length; j++){
            if(rArray[j] != null) {
                int tempID = rArray[j].getRoomID();
                String temp = "  ";
                temp += rArray[j].getNumOfBeds();
                temp += " ";
                temp += rArray[j].getInternetAccess();
                temp += " ";
                temp += rArray[j].getFloor();
                temp += " ";
                temp += rArray[j].getPricePerNight();
                fio.println(tempID + temp);
            }
        }

    }

    public static void createGuest(Scanner input) throws Exception{
        PrintStream output = new PrintStream(new File("guests.txt"));
        Guests g = new Guests();

        System.out.print("Enter the guestID: ");
        g.setGuestID(input.nextInt());
        System.out.print("Enter guests First Name: ");
        g.setFirstName(input.next());

        System.out.print("Enter guests Last Name: ");
        g.setLastName(input.next());


        System.out.println("Enter the address of the Guest: ");
        String em = input.nextLine();
        g.setAddress(input.nextLine());

        System.out.print("Enter the phone number of the Guest: ");
        g.setPhoneNum(input.next());

        gArray[gCount] = g;
        gCount++;

        System.out.println(g);

        // print every guest from the array in the file in a raw format
        for (int j = 0; j < gArray.length; j++){
            if(gArray[j] != null) {
                String tempGuest = gArray[j].getFirstName();
                tempGuest += "  ";
                tempGuest += gArray[j].getLastName();
                tempGuest += " ";
                tempGuest += gArray[j].getAddress();
                tempGuest += " ... ";
                tempGuest += gArray[j].getPhoneNum();
                tempGuest += " ";
                output.println(tempGuest);
            }
        }

    }

    public static void createStaff(Scanner input) throws Exception{

        PrintStream output = new PrintStream(new File("staff.txt"));

        Staff s = new Staff();

        System.out.print("Enter the Staff Title: ");
        s.setTitle(input.next());
        System.out.print("Enter staff's First Name: ");
        String em = input.next();
        s.setFirstName(input.next());

        System.out.print("Enter staff's Last Name: ");

        s.setLastName(input.next());

        System.out.print("Enter the phone number of the Staff member: ");
        s.setPhoneNum(input.next());

        System.out.print("Enter the Staff's salary: ");
        s.setSalary(input.nextDouble());

        sArray[sCount] = s;
        sCount++;

        System.out.println(s);

        // print every staff member from the array in the file in a raw format
        for (int j = 0; j < sArray.length; j++){
            if(sArray[j] != null) {
                String tempStaff = sArray[j].getTitle();
                tempStaff += " ";
                tempStaff += sArray[j].getFirstName();
                tempStaff += " ";
                tempStaff += sArray[j].getLastName();
                tempStaff += " ";
                tempStaff += sArray[j].getPhoneNum();
                tempStaff += " ";
                tempStaff += sArray[j].getSalary();
                output.println(tempStaff);
            }
        }


    }

    public static void createBooking(Scanner input){
        Booking b = new Booking();


    }

    public static void printBill(Scanner input) throws Exception{
        bCount = 0;
        restoreBookings();
        System.out.println("Please select which booking you want to pay for.");
        int bookingNum;
        // checks if the booking id is valid
        // if the booking id is bigger than the number of bookings we have, it will show a message
        for(;;){
            bookingNum = input.nextInt();
            if(bookingNum > bCount){
                System.out.println("You only have "+bCount+" bookings. Please enter again a valid booking id.");
            }else{
                break;
            }
        }
        System.out.println("You have selected booking #"+bookingNum+" : "+bArray[bookingNum-1]);
        System.out.println();
        System.out.println("                      HOTEL PLAZA");
        System.out.println();
        System.out.println();
        System.out.println("Booking details: ");
        System.out.println();
        System.out.println("Start date: "+bArray[bookingNum-1].getStartDate());
        System.out.println("End date: "+bArray[bookingNum-1].getEndDate());
        System.out.println("Number of days: "+bArray[bookingNum-1].getNumOfDays());
        // Get details for the guest
        int i;
        for(i=0;i<gCount;i++){
            if(bArray[bookingNum-1].getGuest() == gArray[i].getGuestID())
                break;
        }
        System.out.println("Guest details: "+gArray[i]);
        // Get details for the room
        for(i=0;i<rCount;i++){
            if(bArray[bookingNum-1].getRoom() == rArray[i].getRoomID())
                break;
        }
        System.out.println("Room details: "+rArray[i]);
        System.out.println();
        System.out.println("Total price: "+(rArray[i].getPricePerNight() * bArray[bookingNum-1].getNumOfDays())+" euro.");
        System.out.println();
        System.out.println("         We hope you had a great time at our hotel !");
        System.out.println("         ///////////////////////////////////////////");
        Thread.sleep(700);
    }



    public static void show()throws Exception{
        rCount = 0;
        gCount = 0;
        sCount = 0;
        bCount = 0;
        restoreRooms();
        Thread.sleep(700);
        restoreStaff();
        Thread.sleep(700);
        restoreGuests();
        Thread.sleep(700);
//        restoreBookings();
        Thread.sleep(700);
    }

   public static void restoreRooms() throws Exception{
       Scanner scan = new Scanner(new File("rooms.txt"));

       boolean flag = false;

       while(scan.hasNextLine()){
           flag = true;
           String line = scan.nextLine();

           Scanner token = new Scanner(line);

           int roomID = token.nextInt();
           int beds = token.nextInt();
           boolean hasInterent = token.nextBoolean();
           double price = token.nextDouble();
           int floor = token.nextInt();

           Room r = new Room(roomID, beds, hasInterent, price, floor);

           rArray[rCount] = r;
           rCount++;

       }

       if (flag){
           System.out.println("We have the following Rooms");
           for (int i = 0; i < rCount; i++)
               System.out.println(rArray[i]);
           System.out.println();

       } else {
           System.out.println("It looks like we need to do some construction and make some new rooms");
           System.out.println();
       }

   }

    public static void restoreStaff() throws Exception{
        Scanner scan = new Scanner(new File("staff.txt"));

        boolean flag = false;

        while(scan.hasNextLine()){
            flag = true;
            String line = scan.nextLine();

            Scanner token = new Scanner(line);

            String title = token.next();
            String fName = token.next();
            String lName = token.next();
            String phone = token.next();
            double salary = token.nextDouble();

            Staff s = new Staff(title, fName, lName, phone, salary);

            sArray[sCount] = s;
            sCount++;

        }

        if (flag){
            System.out.println("We have the following Staff Members");
            for (int i = 0; i < sCount; i++)
                System.out.println(sArray[i]);
            System.out.println();

            System.out.println();
        } else {
            System.out.println("It looks like we need to hire some more people");
            System.out.println();
        }
    }

    public static void restoreGuests() throws Exception{
        Scanner scan = new Scanner(new File("guests.txt"));

        boolean flag = false;

        while(scan.hasNextLine()) {
            flag = true;

            String line = scan.nextLine();
            Scanner token = new Scanner(line);

            int ID = token.nextInt();
            String fName = token.next();
            String lName = token.next();
            String address = token.next();
            String cont = token.next();
            while (!cont.equals(",")) {
                address += cont;
                cont = token.next();
            }
            
            String phone = token.next();

            Guests g = new Guests(ID, fName, lName, address, phone);

            gArray[gCount] = g;
            gCount++;
        }

        if (flag){
            System.out.println("We have the following Guests in our system");
            for (int i = 0; i < gCount; i++)
                System.out.println(gArray[i]);
            System.out.println();
        } else {
            System.out.println("It looks like we need to do some more marketing to get some guests");
            System.out.println();
        }

    }

    public static void restoreBookings()throws Exception{
        // first we read each line of the file
        Scanner fin = new Scanner(new File("bookings.txt"));
        // the flag is used to see if we have any bookings so far in the file
        boolean flag = false;
        while(fin.hasNextLine()){
            flag = true;
            String line = fin.nextLine();
            // then we read each token from the line
            Scanner lineScan = new Scanner(line);
            // the String named "error" helps us ignore all the tokens from the line that we don't need,
            // such as "," or unused words
            String error = lineScan.next();
            Date d1 = new Date();
            d1.setYear(lineScan.nextInt());
            error = lineScan.next();
            d1.setMonth(lineScan.nextInt());
            error = lineScan.next();
            d1.setDay(lineScan.nextInt());
            for(int i=0;i<2;i++){
                error = lineScan.next();
            }
            Date d2 = new Date();
            d2.setYear(lineScan.nextInt());
            error = lineScan.next();
            d2.setMonth(lineScan.nextInt());
            error = lineScan.next();
            d2.setDay(lineScan.nextInt());
            for(int i=0;i<4;i++){
                error = lineScan.next();
            }
            int numDays = lineScan.nextInt();
            for(int i=0;i<2;i++){
                error = lineScan.next();
            }
            int roomID = lineScan.nextInt();
            for(int i=0;i<2;i++){
                error = lineScan.next();
            }
            int guestID = lineScan.nextInt();

            Room rTemp = new Room();
            for(int i=0;i<rCount;i++){
                if(roomID == rArray[i].getRoomID())
                    rTemp = rArray[i];
            }

            Guests gTemp = new Guests();
            for(int i=0;i<gCount;i++){
                if(guestID == gArray[i].getGuestID())
                    gTemp = gArray[i];
            }

            // all we needed from the line is used now to create and add the Booking to the bookings array
            Booking b1 = new Booking(d1,d2,numDays,rTemp,gTemp);
            bArray[bCount] = b1;
            bCount++;
        }
        if(flag == true){
            // we print the array so the user can see all the bookings we have so far, before creating new ones
            System.out.println("So far, our hotel has the following bookings: ");
            for(int i=0;i<bCount;i++)
                System.out.println(bArray[i]);
        }else{
            System.out.println("You didn't create any bookings so far.");
        }
        System.out.println();
    }


}