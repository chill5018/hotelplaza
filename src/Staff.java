import java.net.StandardSocketOptions;

public class Staff {

	private String title;
	private String firstName;
	private String lastName;
	private String phoneNum;
	private double salary;

	public Staff (){
		this(" ", " ", " ", " ", 0.0);
	}

	public Staff(String title, String firstName, String lastName, String phoneNum, double salary){
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNum = phoneNum;
		this.salary = salary;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}


	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getSalary() {
		return this.salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String toString(){
		return String.format("Title: %s, Name: %s, %s, Phone: %s, Salary: %.2f",
				title, firstName, lastName, phoneNum, salary);
	}

}